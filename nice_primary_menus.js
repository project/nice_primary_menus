
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    $('ul.links-menu li').hover( function() { 
      mid = this.id.split('-').pop();
      // only hide this menu if there is a new menu to show
      if ($('#nice-primary-menu-' + mid).size()) {
        $('.nice-primary-menu').hide();
        $('#nice-primary-menu-' + mid).show();
      }
    }, function() { 
//    mid = this.id.split('-').pop();
//    $('#nice-primary-menu-' + mid).hide();
    } );
  }); 
}
