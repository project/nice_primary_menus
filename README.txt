
This module extends the Primary menus with an additional level of secondary
nice menus that hide/show on hover via jquery.  Some theming is required to
enable it.

An option is added to the admin/content/menu interface for selecting which
nice menu to display.  This option is only added to the children of the
primary menu.

NOTE: To use this module, you must do some theming.

Replace the theme('menu_links') with theme('nice_menu_links'),

 OR

If you've already overridden theme_menu_links, add the <li id=...>
as is done in theme_nice_menu_links, i.e.:

  $menu = menu_get_menu();
  foreach ($links as $index => $link) {
    $output .= '<li id="menu-link-'. $menu['path index'][$link['href']] .'" ';

and then call theme('nice_primary_menus');

Author
------
Doug Green
douggreen@douggreenconsulting.com
